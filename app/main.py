from fastapi import FastAPI
from preprocessing import extract_features
from prometheus_fastapi_instrumentator import Instrumentator
from schemas import RequestBody
from utils import load_model

app = FastAPI()
Instrumentator().instrument(app).expose(app)


@app.post("/predict/")
async def predict(body: RequestBody):
    data = body.dict()
    features = extract_features(data)
    model = load_model()
    output = model.predict(features)[0]
    return {"binder" if output == 1 else "non-binder"}
