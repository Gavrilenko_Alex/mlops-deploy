import pandas as pd
from rdkit import Chem
from rdkit.Chem import AllChem
from utils import load_one_hot_encoder


def generate_ecfp(molecule, radius=2, bits=1024):
    return list(AllChem.GetMorganFingerprintAsBitVect(molecule, radius, nBits=bits))


def extract_features(data: dict) -> list[list[float]]:
    df = pd.DataFrame(data, index=[0])
    df["molecule"] = df["molecule_smiles"].apply(Chem.MolFromSmiles)
    df["ecfp"] = df["molecule"].apply(generate_ecfp)

    onehot_encoder = load_one_hot_encoder()
    protein_onehot = onehot_encoder.transform(df["protein_name"].values.reshape(-1, 1))

    features = [
        ecfp + protein
        for ecfp, protein in zip(df["ecfp"].tolist(), protein_onehot.tolist())
    ]
    return features
