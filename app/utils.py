import pickle
from io import BytesIO

import mlflow
import pandas as pd
from fastapi import UploadFile

mlflow.set_tracking_uri("http://127.0.0.1:5001")


def load_model():
    url = "runs:/5d343f0bc1c343098250831091e79390/xgb_model_with_oh"
    model = mlflow.pyfunc.load_model(url)
    return model


def load_one_hot_encoder():
    logged_model = "mlflow-artifacts:/0/7c77dadd0033481b88220e02a1e77d7e/artifacts/one_hot_encoder/onehot_encoder.pkl"
    local_path = mlflow.artifacts.download_artifacts(artifact_uri=logged_model)
    with open(local_path, "rb") as f:
        onehot_encoder = pickle.load(f)
    return onehot_encoder


def read_uploaded_excel(file: UploadFile) -> pd.DataFrame:
    content = BytesIO(file.file.read())
    df = pd.read_csv(content).head(n=1)
    return df
