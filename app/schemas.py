from enum import Enum

from pydantic import BaseModel
from pydantic.v1 import validator
from rdkit import Chem


class Protein(str, Enum):
    BRD4 = "BRD4"
    HSA = "HSA"
    SEH = "sEH"


class RequestBody(BaseModel):
    molecule_smiles: str
    protein_name: Protein

    @validator("molecule_smiles")
    def validate_smiles(cls, value):
        output = Chem.MolFromSmiles(value)
        if output is None:
            raise ValueError("Invalid SMILES string")
        return value

    @validator("protein_name")
    def validate_protein(cls, value):
        if value not in Protein.__members__:
            raise ValueError(f"Invalid protein name: {value}")
        return value
